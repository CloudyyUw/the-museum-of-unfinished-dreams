use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

#[derive(Clone)]
pub struct RamDB {
    data: Arc<Mutex<HashMap<String, String>>>,
}

impl RamDB {
    pub fn new() -> Self {
        Self {
            data: Arc::new(Mutex::new(HashMap::new())),
        }
    }
    pub fn insert(&self, index: String, value: String) -> bool {
        let mut data = self.data.lock().unwrap();
        data.insert(index, value);
        true
    }
    pub fn get(&self, index: String) -> Option<String> {
        let data = self.data.lock().unwrap();
        match data.get(&index) {
            Some(value) => Some(value.clone()),
            None => None,
        }
    }
    pub fn drop(&self, index: String) {
        let mut data = self.data.lock().unwrap();
        data.remove(&index).unwrap();
    }
    pub fn edit(&self, index: String, new_val: String) -> bool {
        self.insert(index, new_val)
    }
    pub fn has_key(&self, key: String) -> bool {
        let data = self.data.lock().unwrap();
        data.contains_key(&key)
    }
    // pub fn shrink_to(&self, size: usize) {
    //     let mut data = self.data.lock().unwrap();
    //     data.shrink_to(size);
    // }
    // pub fn shrink_fit(&self) {
    //     let mut data = self.data.lock().unwrap();
    //     data.shrink_to_fit();
    // }
    // pub fn size(&self) -> usize {
    //     let data = self.data.lock().unwrap();
    //     data.len()
    // }
    // pub fn max_size(&self) -> usize {
    //     let data = self.data.lock().unwrap();
    //     data.capacity()
    // }
}
