use std::{
    io::{Read, Write},
    net::{Shutdown, TcpListener, TcpStream},
};

mod crud_hander;
mod ramdb;
use ramdb::RamDB;

const BUFFERSIZE: usize = 1048576; // 1 MB

fn handler(mut stream: TcpStream, db: &mut RamDB) {
    stream.write(b"Connected\n").unwrap();
    let mut data = [0 as u8; BUFFERSIZE];
    while match stream.read(&mut data) {
        Ok(_size) => {
            if _size > 0 {
                crud_hander::parse_input(&mut stream, data, db);
                data = [0; BUFFERSIZE];
            }
            true
        }
        Err(e) => {
            println!("Err: {:?}", e);
            stream.shutdown(Shutdown::Both).unwrap();
            false
        }
    } {}
}

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:5566")?;
    let mut ramdb = RamDB::new();

    println!("Addr: TCP 127.0.0.1:5566");

    for stream in listener.incoming() {
        match stream {
            Err(e) => println!("Connection stream error: {:?}", e),
            Ok(stream) => handler(stream, &mut ramdb),
        }
    }

    Ok(())
}
