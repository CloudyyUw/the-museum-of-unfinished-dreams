import { Component, Show, createSignal } from 'solid-js';
import { useParams } from '@solidjs/router';
import pocketbase from './utils/dbConnection';
import markdownToHtml from './utils/markdownToHtml';

import Nav from './components/nav';
import { IoArrowBackOutline } from 'solid-icons/io';

import './css/markdown.css';

const Post: Component = () => {
    if (pocketbase.authStore.token.length > 0 == false) {
        location.href = '/login';
    }

    const { board_id, post_id } = useParams(),
        [isLoading, setLoading] = createSignal(true),
        [postData, setPostData] = createSignal();

    (async () => {
        const post_data = await pocketbase.collection('posts').getOne(post_id, {
            expand: 'author,parent_board',
        });
        console.log(post_data);

        setLoading(false);
    })();

    return (
        <>
            <Nav />

            <div class="container mx-auto my-auto px-10 py-20">
                <Show when={isLoading() == true}>
                    <div class="flex justify-center items-center h-screen flex-col">
                        <span class="loading loading-spinner text-primary"></span>
                        Loading..
                    </div>
                </Show>
                <Show when={isLoading() == false}>
                    <a class="btn btn-sm" href={`/board/${board_id}`}>
                        <IoArrowBackOutline /> Back
                    </a>
                    <div class="mt-4 w-full bg-base-200 rounded-md py-4 px-5"></div>
                </Show>
            </div>
        </>
    );
};

export default Post;
