import { remark } from 'remark';
import html from 'remark-html';
import dompurify from 'dompurify';

export default async function markdownToHtml(md: string) {
    const res = await remark().use(html).process(dompurify.sanitize(md));
    return res.toString();
}
